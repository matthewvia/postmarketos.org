title: "Various aarch64 segfaults"
date: 2022-09-21
---

Due to a problem with an edge builder for aarch64, there were segfaults on
Alpine edge for aarch64. For example gtk4 was segfaulting, and caused Phosh to
crash often. The gtk4 package has been rebuilt, but if you see more packages
segfaulting then make sure you updated your packages and try rebuilding the
package first before further investigating and reporting an issue.

Related issue: [aports#14189](https://gitlab.alpinelinux.org/alpine/aports/-/issues/14189)
