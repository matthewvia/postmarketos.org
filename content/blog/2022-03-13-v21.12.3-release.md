title: "v21.12 Service Pack 3"
title-short: "v21.12 SP3"
date: 2022-03-13
preview: "2022-03/pinephone_in_spring.jpg"
---


[![](/static/img/2022-03/pinephone_in_spring.jpg){: class="wfull border"}](/static/img/2022-03/pinephone_in_spring.jpg)

Spring is almost here, in the northern hemisphere, and that means it is time
for another monthly service pack update for postmarketOS v21.12! This update
brings a few bug fixes and package upgrades from edge to the stable release.

## Contents

* Dino ~0.3 upgraded to the latest version in @mimi89999's
  [feature/handy](https://github.com/mimi89999/dino/tree/feature/handy) branch,
  with support for an adaptive UI.

* <del>[Squeekboard 1.16.0](https://gitlab.gnome.org/World/Phosh/squeekboard/-/commit/d49ce45de0956432cef9b957f806d9377fee4bc0),
  with many new language translations.</del> (Update 2022-03-14: reverted due
  to build problems, [pmb#2110](https://gitlab.com/postmarketOS/pmbootstrap/-/issues/2110))

* Portfolio upgraded to [0.9.3](https://github.com/tchx84/Portfolio/blob/master/CHANGELOG.md#0913-2022-02-17),
  bringing support for mounting/managing external drives. Useful for using usb
  drives with a docked device, or SD cards on devices that support those.

* Fixed alarms not waking devices from suspend
  ([pma#1189](https://gitlab.com/postmarketOS/pmaports/-/issues/1189)),
  the daemon (`waked`) responsible for scheduling the wake event was not set to
  start by default.

* SDM845 Linux kernel upgraded to [5.16.12](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2986)

* MSM8996 Linux kernel upgraded to [5.16.12](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2979)

* N900 Linux kernel upgraded to [5.15.25](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2964)

* PinePhone keyboard kernel driver is now built as module
  ([pma#1444](https://gitlab.com/postmarketOS/pmaports/-/issues/1444))



## How to get it

Find the most recent images at our [download page](/download/). Existing users
of the v21.12 release will receive this service pack automatically on their
next system update. If you read this blog post right after it was published, it
may take a bit until binary packages are available.

Thanks to everybody who made this possible, especially our amazing community
members and upstream projects.

## Comments
* [Mastodon](https://fosstodon.org/web/@postmarketOS/107950172499731125)
* [Lemmy](https://lemmy.ml/post/192281)
<small>
* [Reddit](https://www.reddit.com/r/postmarketOS/comments/tdal3t/postmarketos_v2112_service_pack_3/)
* [Twitter](https://twitter.com/postmarketOS/status/1503045655614439425)
</small>
